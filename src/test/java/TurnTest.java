//package test.java;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
//import main.java.*;

public class TurnTest {

    private Turn turnT;
    private int numOfPlayers;

    @Before
    public void setup(){
        numOfPlayers = 3;
        turnT = new Turn(numOfPlayers);
    }

    @Test
    public void add_position_succeed(){
        Position newPos = new Position(5,5);
        try {
            turnT.addPosition(newPos, 0);
        } catch (final MyInvalidParameterException e){
            System.out.println(e.message());
        }
        Assert.assertEquals(newPos, turnT.getPos()[0]);
    }

    @Test
    public void add_position_player_not_exist(){
        try {
            turnT.addPosition(new Position(5, 5), 3);
        } catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Player"+"\n"+"Player does not exist!", e.message());
        }
    }

    @Test
    public void get_position(){
        Position newPos = new Position(0,0);
        try {
            turnT.addPosition(newPos, 1);
        } catch (final MyInvalidParameterException e){
            System.out.println(e.message());
        }
        Assert.assertEquals(newPos, turnT.getPosition(1));
    }

    @After
    public void teardown(){
        numOfPlayers = 0;
        turnT = null;
    }

}
