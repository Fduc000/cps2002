import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;


public class TeamPositionObserverTest {

    private Game myGame;
    private Map mapGenerator;
    private MapType myMap;
    private char[][][] arrayOfMaps;

    @Before
    public void setup(){

        mapGenerator = new Map();

        int numOfPlayers = 6;
        int MapSizeX = 5;
        int MapSizeY = 5;

        arrayOfMaps = new char[numOfPlayers][MapSizeX][MapSizeY];

        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        myMap = mapGenerator.getMapType(Type.Hazardous,MapSizeX, MapSizeY);
        myGame = new Game(myMap, MapSizeX, MapSizeY, numOfPlayers);

        myGame.setPlayers(new Player[numOfPlayers]);

        for (int x = 0; x < numOfPlayers; x++){
            myGame.getPlayers()[x] = new Player();
            myGame.getPlayers()[x].setMap(myMap.getGrid());
        }

        for (int i = 0; i < arrayOfMaps.length; i++){
            for (int s = 0; s < arrayOfMaps[i].length; s++) {
                for (int t = 0; t < arrayOfMaps[i][0].length; t++) {
                    char inpt = arrayOfMaps[i][s][t];
                    {myGame.getPlayers()[i].getMap()[s][t] = inpt;}
                }
            }
        }

        myGame.getPlayers()[0].setTeamNo(0);
        myGame.getPlayers()[1].setTeamNo(1);
        myGame.getPlayers()[2].setTeamNo(0);
        myGame.getPlayers()[3].setTeamNo(1);
        myGame.getPlayers()[4].setTeamNo(0);
        myGame.getPlayers()[5].setTeamNo(1);


    }

    @Test
    public void mapChangesForTeam(){
        //call update
        myGame.getTeamObs().update(myGame.getPlayers(), myGame.getPlayers()[0].getTeamNo(), 2, 3, arrayOfMaps);
        Assert.assertArrayEquals(myGame.getPlayers()[2].getMap(),myGame.getPlayers()[0].getMap());
        Assert.assertArrayEquals(myGame.getPlayers()[4].getMap(),myGame.getPlayers()[0].getMap());
    }

    @Test
    public void mapDoesNotChangeForOthers(){
        //call update
        myGame.getTeamObs().update(myGame.getPlayers(), myGame.getPlayers()[0].getTeamNo(), 2, 3, arrayOfMaps);
        Assert.assertThat(myGame.getPlayers()[1].getMap(), IsNot.not(IsEqual.equalTo(myGame.getPlayers()[0].getMap())));
        Assert.assertThat(myGame.getPlayers()[3].getMap(), IsNot.not(IsEqual.equalTo(myGame.getPlayers()[0].getMap())));
        Assert.assertThat(myGame.getPlayers()[5].getMap(), IsNot.not(IsEqual.equalTo(myGame.getPlayers()[0].getMap())));
    }

    @After
    public void teardown(){
        myGame = null;
        mapGenerator = null;
        myMap = null;

    }
}
