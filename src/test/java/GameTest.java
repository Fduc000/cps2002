//package test.java;

import org.junit.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class GameTest {

    private Game myGame;
    private Map mapGenerator;
    private MapType myMap;

    @Before
    public void setup() {

        mapGenerator = new Map();

        int numOfPlayers = 2;
        int MapSizeX = 5;
        int MapSizeY = 5;

        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        myMap = mapGenerator.getMapType(Type.Hazardous,MapSizeX, MapSizeY);
        myGame = new Game(myMap, MapSizeX, MapSizeY, numOfPlayers);

        myGame.setPlayers(new Player[numOfPlayers]);
        myGame.getPlayers()[0] = new Player();
        myGame.getPlayers()[1] = new Player();

        myGame.getPlayers()[0].map = myMap.getGrid();
        myGame.getPlayers()[1].map = myMap.getGrid();

    }

    @Test
    public void no_of_players_is_good(){
        Assert.assertEquals(true, myGame.setNumPlayers(3));
    }

    @Test
    public void no_of_players_is_low(){
        Assert.assertEquals(false, myGame.setNumPlayers(0));
    }

    @Test
    public void no_of_players_is_high(){
        Assert.assertEquals(false, myGame.setNumPlayers(20));
    }

    @Test
    public void get_players(){
        Assert.assertNotNull(myGame.getPlayers());
    }

    @Test
    public void set_num_players_correct(){
        Assert.assertEquals(true, myGame.setNumPlayers(4));
    }

    @Test
    public void set_num_players_large(){
        Assert.assertEquals(false, myGame.setNumPlayers(10));
    }

    @Test
    public void set_num_players_small(){
        Assert.assertEquals(false, myGame.setNumPlayers(0));
    }

    @Test
    public void copy_paste_map(){
        char[][] tempMap = new char[myMap.getMapSize()[0]][myMap.getMapSize()[1]];
        char[][] tempMap2 = myGame.copy_paste_map(myMap.getGrid(), tempMap);
        Assert.assertArrayEquals(myMap.getGrid(), tempMap2);
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //The following tests only succeed when no user input is involved
    //Therefore to test the below, please remove the comment slashes
    // and remove any user-input method calls from the actual methods

//    @Test
//    public void turns_not_empty(){
//        myGame.startGame(myGame.getPlayers().length, mapGenerator);
//        Assert.assertNotNull(myGame.getTurns());
//    }
//
//    @Test
//    public void first_positions_grass(){
//        myGame.startGame(myGame.getPlayers().length, mapGenerator);
//        for (int i = 0; i < myGame.getPlayers().length; i++) {
//            int posX = myGame.getTurns()[0].getPosition(i).x;
//            int posY = myGame.getTurns()[0].getPosition(i).y;
//            try{
//                Assert.assertEquals('g', mapGenerator.getTileType(posX, posY, myGame.map.getGrid()));
//            } catch (final MyInvalidParameterException e) {
//                System.out.println(e.message());
//            }
//        }
//    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @After
    public void teardown() {
        myGame = null;
        mapGenerator = null;
        myMap = null;
    }


}
