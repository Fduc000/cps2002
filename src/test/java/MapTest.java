import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;

public class MapTest {

    private MapType mapT;
    private Map map;

    @Before
    public void setup() {
        map = new Map();
    }

    @Test
    public void getting_type_Safe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        MapType MyMap = map.getMapType(Type.Safe, 22, 30);
        Assert.assertEquals(MyMap.getType(),Type.Safe);
    }

    @Test
    public void getting_type_Hazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        MapType MyMap = map.getMapType(Type.Hazardous, 22, 30);
        Assert.assertEquals(MyMap.getType(),Type.Hazardous);
    }

    @Test
    public void mapSize_not_empty_ForSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(0 ,0,1,Type.Safe);
        }catch(final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is zero or negative!",e.message());
        }
    }

    @Test
    public void mapSize_not_empty_ForHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(0 ,0,1,Type.Hazardous);

        }catch(final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is zero or negative!",e.message());
        } // check coverage here
    }

    @Test
    public void mapSize_isTooSmall_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(1, 4, 6, Type.Safe);
        } catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is not within required limits!", e.message());
        }
    }

    @Test
    public void mapSize_isTooSmall_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(1, 4, 6, Type.Hazardous);
        } catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is not within required limits!", e.message());
        }
    }

    @Test
    public void mapSize_isTooSmallForPlayers_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(6, 6, 6, Type.Safe);
        } catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is not within required limits!", e.message());
        }
    }

    @Test
    public void mapSize_isTooSmallForPlayers_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(6, 6, 6, Type.Hazardous);
        } catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is not within required limits!", e.message());
        }
    }

    @Test
    public void mapSize_isTooLarge_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(78, 30, 7, Type.Safe);
        } catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is not within required limits!", e.message());
        }
    }

    @Test
    public void mapSize_isTooLarge_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(78, 30, 7, Type.Hazardous);
        } catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is not within required limits!", e.message());
        }
    }

    @Test
    public void mapSize_isWithinLimit_forSafe_EqualSize(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            MapType MySafeMap = map.getMapType(Type.Safe, 22, 30);
            MapType MySupposedSafeMap = map.setMapSize(22, 30, 2, Type.Safe);
            Assert.assertArrayEquals(MySafeMap.getMapSize(),MySupposedSafeMap.getMapSize());
        } catch (final MyInvalidParameterException e){
            System.out.println(e.message());
        }
    }

    @Test
    public void mapSize_isWithinLimit_forSafe_EqualGrid(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            MapType MySafeMap = map.getMapType(Type.Safe, 22, 30);
            MapType MySupposedSafeMap = map.setMapSize(22, 30, 2, Type.Safe);
            Assert.assertArrayEquals(MySafeMap.getGrid(), MySupposedSafeMap.getGrid());
        } catch (final MyInvalidParameterException e){
            System.out.println(e.message());
        }
    }

    @Test
    public void mapSize_NotNegative_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(-7 ,-20,3,Type.Safe);

        }catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is zero or negative!", e.message());
        }
    }

    @Test
    public void mapSize_NotNegative_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(-7 ,-20,3,Type.Hazardous);

        }catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is zero or negative!", e.message());
        }
    }

    @Test
    public void no_players_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(9 ,9,0,Type.Safe);

        }catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Number of players"+"\n"+"There must be players", e.message());
        }
    }

    @Test
    public void no_players_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(9 ,9,0,Type.Hazardous);

        }catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Number of players"+"\n"+"There must be players", e.message());
        }
    }

    @Test
    public void too_much_players_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(9 ,9,9,Type.Safe);

        }catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Number of players"+"\n"+"Too much players!", e.message());
        }
    }

    @Test
    public void too_much_players_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(9 ,9,9,Type.Hazardous);

        }catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Number of players"+"\n"+"Too much players!", e.message());
        }
    }

    @Test
    public void map_too_small_for_few_player(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            map.setMapSize(3 ,3,2,Type.Hazardous);

        }catch (final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Map size is not within required limits!", e.message());
        }
    }

    @Test
    public void testingGenerate_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        MapType mapSafe = map.getMapType(Type.Safe,10,10);
        mapSafe.generate(10,10,2);
    }

    @Test
    public void testingGenerate_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        MapType mapSafe = map.getMapType(Type.Hazardous,10,10);
        mapSafe.generate(10,10,2);
    }

    @Test
    public void testingGenerateWithLargeSize_forSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Safe,58,77);
        mapT = mapT.generate(58,77,5);
        Assert.assertArrayEquals(mapT.getGrid(), mapT.getGrid());
    }

    @Test
    public void testingGenerateWithLargeSize_forHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Hazardous,58,77);
        mapT = mapT.generate(58,77,5);
        Assert.assertArrayEquals(mapT.getGrid(), mapT.getGrid());
    }

    @Test
    public void testingGenerateWithSmallSize(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Safe,0,0);
        mapT = mapT.generate(0,0,4);
        Assert.assertArrayEquals(mapT.getGrid(), mapT.getGrid());
    }

    @Test
    public void testingGenerateWithSmallSizeForManyPlayers(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Safe,5,5);
        mapT = mapT.generate(5,5,8);
        Assert.assertArrayEquals(mapT.getGrid(), mapT.getGrid());
    }

    @Test
    public void getTileType_forTileTooFar(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Safe,58,77);
        try {
            map.getTileType(58,77,mapT.getGrid());

        }catch(final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Tile does not exist!", e.message());
        }
    }

    @Test
    public void getTileType_forTileOutOfBounds(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Safe,40,40);

        try {
            map.getTileType(49,45,mapT.getGrid());

        }catch(final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Tile is out of bounds!", e.message());
        }
    }

    @Test
    public void getTileType_forNegativeTiles(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Safe,40,40);
        try {
            map.getTileType(-5,-5,mapT.getGrid());

        }catch(final MyInvalidParameterException e){
            Assert.assertEquals("Invalid parameter:: "+"Map size"+"\n"+"Tile does not exist!", e.message());
        }
    }

    @Test
    public void getTileType_forExistingTile(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapT = map.getMapType(Type.Safe,5,5);
        mapT = mapT.generate(5,5,3);

        mapT.getGrid()[0][0] = 'c';

        try {
            char check = map.getTileType(2, 1, mapT.getGrid());
            Assert.assertTrue(check=='g'||check=='w'||check=='t'||check=='c');

        } catch(final MyInvalidParameterException e){
            System.out.println(e.message());
        }
    }

    @Test
    public void Using_GetInstance(){
        try{
            SafeMap.getInstance(6,8);
            Assert.assertNotNull(SafeMap.getmap());

        } catch (OnlyOneMapException e){
            System.out.println(e.message());
        }

    }

    @Test
    public void OnlyOneTypeOfMapCanBeCreated_ForSafe(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            SafeMap.getInstance(6,9);

            HazardousMap.getInstance(8,13);

        }catch(final OnlyOneMapException e){
            Assert.assertEquals("Map of type Safe Map already created for this Game!\n",e.message());
        }
    }

    @Test
    public void OnlyOneTypeOfMapCanBeCreated_ForHazardous(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        try {
            HazardousMap.getInstance(7,5);
            SafeMap.getInstance(8,9);

        }catch(final OnlyOneMapException e){
            Assert.assertEquals("Map of type Hazardous Map already created for this Game!\n",e.message());
        }
    }


    @After
    public void teardown() {
        mapT = null;
    }
}
