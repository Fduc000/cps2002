//package test.java;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
//import main.java.*;

public class PlayerTest {

    private Player player;
    private Map mapGenerator;
    private MapType newMap;


    @Before
    public void setup() {
        player = new Player();
        player.position = new Position(5,5);

    }

    @Test
    public void setting_map_position(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        player.setMapPosition('w',2,3);
        Assert.assertEquals('w', player.getMap()[2][3]);

    }

    @Test
    public void direction_not_empty(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        try {
            this.player.move(' ');

        }catch(final IllegalStateException e){
            Assert.assertEquals("Message is not consistent","Direction is empty!",e.getMessage());
        }
    }

    @Test
    public void direction_correct_up(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals("Moved up!", player.move('U'));
    }

    @Test
    public void direction_correct_down(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals("Moved down!", player.move('D'));
    }

    @Test
    public void direction_correct_left(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals("Moved left!", player.move('L'));
    }

    @Test
    public void direction_correct_right(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();

        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals("Moved right!", player.move('R'));
    }

    @Test
    public void direction_wrong(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals("Invalid direction!", player.move('A'));
    }

    @Test
    public void direction_out_of_map_big(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        player.position.x = 8;
        player.position.y = 8;
        Assert.assertEquals("Moved right!\nSomething has happened...\n", player.move('R'));
        player.position.x = 5;
        player.position.y = 5;
    }

    @Test
    public void direction_out_of_map_small(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        player.position.x = 0;
        player.position.y = 0;
        Assert.assertEquals("Moved up!\nSomething has happened...\n", player.move('U'));
        player.position.x = 5;
        player.position.y = 5;
    }

    @Test
    public void direction_hit_water(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        newMap.getGrid()[4][5] = 'w';
        Assert.assertEquals("Moved up!", player.move('U'));
        newMap.getGrid()[4][5] = '\0';
    }

    @Test
    public void direction_hit_treasure(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        newMap.getGrid()[4][5] = 't';
        Assert.assertEquals("Moved up!", player.move('U'));
        newMap.getGrid()[4][5] = '\0';
    }

    @Test
    public void position_in_map(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals(true, player.setPosition(new Position(0,0)));
    }

    @Test
    public void position_not_negative(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals(false, player.setPosition(new Position(-5,2)));
    }

    @Test
    public void position_not_in_map(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertEquals(false, player.setPosition(new Position(11,11)));

    }

    @Test
    public void to_string_test(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertNotNull(player.toString());
    }

    @Test
    public void gettingtheMap(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);
        player.map = newMap.getGrid();

        Assert.assertNotNull(player.getMap());
    }

    @Test
    public void settingTheMap(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);

        player.map = null;
        player.setMap(newMap.getGrid());
        Assert.assertNotNull(player.map);
    }

    @Test
    public void gettingTeamNumber(){
        HazardousMap.setmapTONULL();
        SafeMap.setmapTONULL();
        mapGenerator = new Map();
        newMap = mapGenerator.getMapType(Type.Safe,9,9);

        //player.teamNo = null;
        player.setTeamNo(2);
        Assert.assertEquals(2,player.getTeamNo());
    }

    @After
    public void teardown() {
        player = null;
        newMap = null;
        mapGenerator=null;
    }



}
