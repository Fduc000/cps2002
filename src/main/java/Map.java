public class Map {

    //this class is now used for the factory method

    public Map(){ } // constructor

    //creates map instance acording to type specified by user
    public MapType getMapType(Type mapType, int x, int y){
        try {

            if (mapType == Type.Safe) {
                return SafeMap.getInstance(x, y);

            } else if (mapType == Type.Hazardous) {
                return HazardousMap.getInstance(x, y);
            }

        }catch(OnlyOneMapException e){
            System.out.println(e.message());
        }

        return null;
    }

    //sets the size of the map
    public MapType setMapSize(int x, int y,int numOfPlayers, Type type) throws MyInvalidParameterException{

        //number of player conditions to check valid parameter input
        if (numOfPlayers==0){
            throw new MyInvalidParameterException("There must be players", "Number of players");
        }

        if (numOfPlayers>8){
            throw new MyInvalidParameterException("Too much players!", "Number of players");
        }

        //applying required conditions to verify the sizes inputted by user can be used to create the map
        if (x <= 0 || y <= 0) {
            throw new MyInvalidParameterException("Map size is zero or negative!", "Map size");
        }

        if(numOfPlayers<5) {
            if (x < 5 || y < 5 || x > 50 || y > 50) {
                throw new MyInvalidParameterException("Map size is not within required limits!", "Map size");
            }
        }
        else{
            if (x < 8 || y < 8 || x > 50 || y > 50) {
                throw new MyInvalidParameterException("Map size is not within required limits!", "Map size");
            }
        }

        //setting the sizes
        if(type == Type.Safe){
            SafeMap.getmap().getMapSize()[0] = x;
            SafeMap.getmap().getMapSize()[1] = y;

            return SafeMap.getmap();
        } else {
            HazardousMap.getmap().getMapSize()[0] = x;
            HazardousMap.getmap().getMapSize()[1] = y;

            return HazardousMap.getmap();
        }
    }

    //getting the type of a specified tile
    public char getTileType(int x, int y, char[][] newMap) throws MyInvalidParameterException{

        //checking that the tile exists
        if(x<0 || y<0 || x>=50 || y>=50)
            throw new MyInvalidParameterException("Tile does not exist!", "Map size");

        if(x>newMap.length || y>newMap[0].length)
            throw new MyInvalidParameterException("Tile is out of bounds!", "Map size");

        //returning according to tile type
        if (newMap[x][y]=='w')
            return 'w';

        else if (newMap[x][y]=='t')
            return 't';

        else if (newMap[x][y] == 'c')
            return 'c';

        else
            return 'g';
    }
}
