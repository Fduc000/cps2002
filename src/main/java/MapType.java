public interface MapType {

    //methods to override
    MapType generate(int x, int y, int numOfPlayers);
    int[] getMapSize();
    char[][] getGrid();
    Type getType();

    @Override
    String toString();

}
