//import java.io.IOException;

public class SafeMap implements MapType {

    //safe map constructor
    private SafeMap(int x, int y){
        this.size = new int[2];
        this.size[0] = x;
        this.size[1] = y;
        this.grid = new char [size[0]][size[1]];
        this.type = Type.Safe;
    }

    //creating an instance of safe map and setting it to null
    //used for singleton design pattern
    //only one instance can be created
    private static SafeMap safemap = null;

    private int size[];
    private char grid[][];
    public Type type;

    //creates the map instance
    public static SafeMap getInstance(int x, int y) throws OnlyOneMapException{

        //creating the map instance
        if (safemap == null) {
            safemap = new SafeMap(x, y);

            //throwing exception if a hazardous map already exists
            //since we can only create one map instance
            if(HazardousMap.getmap()!=null){
                throw new OnlyOneMapException("Hazardous Map");
            }

        } else {
            //throwing an exception if a safe map already exists
            throw new OnlyOneMapException("Safe Map");
        }

        return safemap;
    }

    //returns the map instance
    public static SafeMap getmap(){
        return safemap;
    }

    //sets the map instance to null / deletes the map
    //to use for tests so that at the beginning of every test the map is set to null
    public static void setmapTONULL(){
        safemap = null;
    }

    //returns the map type
    @Override
    public Type getType(){
        return Type.Safe;
    }

    //return the map size
    @Override
    public int[] getMapSize(){
        return this.size;
    }

    //returns the map grid
    @Override
    public char[][] getGrid(){
        return this.grid;
    }

    //generates the map according to its type
    @Override
    public MapType generate(int x, int y, int numOfPlayers){

        MapType newMap;
        //instance used to access the common methods of both types of class
        //this class is used to implement the factory design onto our code structure
        Map mapGenerator = new Map();

        try {
            //setting the size of the map
            newMap = mapGenerator.setMapSize(x, y, numOfPlayers,Type.Safe);

            //catching the possibly throw exception from the called method
        } catch (final MyInvalidParameterException e){
            System.out.println(e.getMessage());

            //initializing the new map with 0 sizes if map size setting fails
            //newMap variable must be initialized
            // to avoid NullPointerException from being thrown for getting map size
            return new SafeMap(0,0);
        }

        //setting 10% of the map to be water
        int TotalArea = newMap.getMapSize()[0]*newMap.getMapSize()[1];
        int waterParts = (int)Math.floor(TotalArea*0.10);

        int xWater, yWater;

        for(int i=0; i< waterParts; i++) {
            xWater = (int) Math.floor(Math.random() * (newMap.getMapSize()[0])); //for the x
            yWater = (int) Math.floor(Math.random() * (newMap.getMapSize()[1])); //for the y

            //setting the specified tiles to water tiles
            {newMap.getGrid()[xWater][yWater]='w';}
        }

        //random x and random y - to place the treasure
        int xTreasure = (int)Math.floor(Math.random() * (newMap.getMapSize()[0])); //for the x
        int yTreasure = (int)Math.floor(Math.random() * (newMap.getMapSize()[1])); //for the y

        //setting the specified tile to the treasure tile
        {newMap.getGrid()[xTreasure][yTreasure]='t';}

        return newMap;
    }
}
