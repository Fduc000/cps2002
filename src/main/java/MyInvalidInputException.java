public class MyInvalidInputException extends Exception {

    private String msg;

    public MyInvalidInputException(String message){
        this.msg = message;
    }

    //outputs details on what went wrong
    public String message(){
        return "Invalid input:: "+msg;
    }
}
