public interface Observer {
    void update(Player[] players, int teamNo, int posX, int posY, char[][][] arrayOfMaps);
}
