public class OnlyOneMapException extends Exception {

    private String type;

    public OnlyOneMapException(String typeOfMap){
        this.type = typeOfMap;
    }

    //outputting that the map instance has already been created, specifying the type it is in
    public String message(){
        return "Map of type "+type+" already created for this Game!\n";
    }
}
