import java.util.Random;

public class HazardousMap implements MapType {

    //hazardous map constructor
    private HazardousMap(int x, int y){
        this.size = new int[2];
        this.size[0] = x;
        this.size[1] = y;
        this.grid = new char [size[0]][size[1]];
        this.type = Type.Hazardous;

    }

    //creating an instance of hazardous map and setting it to null
    //used for singleton design pattern
    //only one instance can be created
    private static HazardousMap hazardousmap = null;

    private int size[];
    private char grid[][];
    public Type type;

    //creates the map instance
    public static HazardousMap getInstance(int x, int y) throws OnlyOneMapException{

        //creating the map instance
        if (hazardousmap == null) {
            hazardousmap = new HazardousMap(x, y);

            //throwing exception if a safe map already exists
            //since we can only create one map instance
            if(SafeMap.getmap()!=null){
                throw new OnlyOneMapException("Safe Map");
            }
        }
        else{
            //throwing an exception if a hazardous map already exists
            throw new OnlyOneMapException("Hazardous Map");
        }

        return hazardousmap;
    }

    //returns the map instance
    public static HazardousMap getmap(){
        return hazardousmap;
    }

    //sets the map instance to null / deletes the map
    //to use for tests so that at the beginning of every test the map is set to null
    public static void setmapTONULL(){
        hazardousmap = null;
    }

    //returns the map type
    @Override
    public Type getType(){
        return Type.Hazardous;
    }

    //returns the map size
    @Override
    public int[] getMapSize(){
        return this.size;
    }

    //returns the map grid
    @Override
    public char[][] getGrid(){
        return this.grid;
    }

    //generates the map according to its type
    @Override
    public MapType generate(int x, int y, int numOfPlayers){

        MapType newMap;
        //instance used to access the common methods of both types of class
        //this class is used to implement the factory design onto our code structure
        Map mapGenerator = new Map();

        try {
            //setting the size of the map
             newMap = mapGenerator.setMapSize(x, y, numOfPlayers, Type.Hazardous);

             //catching the possibly throw exception from the called method
        } catch (final MyInvalidParameterException e){
            System.out.println(e.getMessage());

            //initializing the new map with 0 sizes if map size setting fails
            //newMap variable must be initialized
            // to avoid NullPointerException from being thrown for getting map size
            return new HazardousMap(0,0);
        }

        //setting 25-35% of the map to be water
        Random ran = new Random();
        double ranNum = 0.25 + (0.35 - 0.25) * ran.nextDouble();

        int TotalArea = newMap.getMapSize()[0] * newMap.getMapSize()[1];
        int waterParts = (int) Math.floor(TotalArea * ranNum); // amount of tiles to be set to water

        int xWater, yWater;

        for(int i=0; i < waterParts; i++) {

            xWater = (int) Math.floor(Math.random() * (newMap.getMapSize()[0])); //for the x
            yWater = (int) Math.floor(Math.random() * (newMap.getMapSize()[1])); //for the y

            //setting the specified tiles to water tiles
            {newMap.getGrid()[xWater][yWater]='w';}
        }

        //random x and random y - to place the treasure
        int xTreasure = (int)Math.floor(Math.random() * (newMap.getMapSize()[0])); //for the x
        int yTreasure = (int)Math.floor(Math.random() * (newMap.getMapSize()[1])); //for the y

        //setting the found tile to be the treasure tile
        {newMap.getGrid()[xTreasure][yTreasure]='t';}

        return newMap;
    }
}
