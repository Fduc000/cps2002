//added this class since <> operator does not work on jenkins
public class Turn {

    //stores the positions of all the players in this turn
    private Position pos[];

    public Turn(int numOfPlayers){
        this.pos = new Position[numOfPlayers];
    } // constructor

    //gets the array of positions
    public Position[] getPos() {
        return pos;
    }

    //adds a position to the list, specifying the position and which player
    public void addPosition(Position position, int player) throws MyInvalidParameterException {

        if (player >= this.pos.length)
            throw new MyInvalidParameterException("Player does not exist!", "Player");

        this.pos[player] = position;
    }

    //getting the position of a specified player in this turn
    public Position getPosition(int player){
        return pos[player];
    }
}
