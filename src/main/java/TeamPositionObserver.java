public class TeamPositionObserver implements Observer {

    @Override
    public void update(Player[] players, int playerNo, int posX, int posY, char[][][] arrayOfMaps){

        //updating player's maps to display the positions of the whole team

        for (int i = 0; i < players.length; i++){
            if (players[i].teamNo == players[playerNo].teamNo){
                arrayOfMaps[i][posX][posY] = 'c';
                players[i].setMap(arrayOfMaps[i]);
            }
        }
    }
}
