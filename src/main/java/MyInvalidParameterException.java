public class MyInvalidParameterException extends Exception{

    private String varName;
    private String msg;

    public MyInvalidParameterException(String message, String variable){
        this.msg = message;
        this.varName = variable;
    }

    //outputs details on what went wrong, including the name of the invalid parameter
    public String message(){
        return "Invalid parameter:: "+varName+"\n"+msg;
    }
}
