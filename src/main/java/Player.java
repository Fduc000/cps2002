public class Player {

    public Position position; // current position of player
    public char[][] map; // map representation
    public int teamNo; // team number of team the player is on

    public Player(){ } // constructor

    //gets the team the player is on
    public int getTeamNo() {
        return teamNo;
    }

    //sets the team number for the player
    public void setTeamNo(int teamNo) {
        this.teamNo = teamNo;
    }

    //gets the player's map representation
    public char[][] getMap() {
        return map;
    }

    //sets the player's map representation
    public void setMap(char[][] map) {
        this.map = map;
    }

    //setting a certain position in the player's map to some value (used in testing)
    public void setMapPosition(char type, int x, int y) {
        this.map[x][y] = type;
    }

    //moves the player in some direction specified
    public String move(char direction){

        boolean status = true;
        String message;

        switch (direction){
            case 'U':
                status = setPosition(new Position(this.position.x-1, this.position.y));
                message = "Moved up!";
                break;
            case 'D':
                status = setPosition(new Position(this.position.x+1, this.position.y));
                message = "Moved down!";
                break;
            case 'L':
                status = setPosition(new Position(this.position.x, this.position.y-1));
                message = "Moved left!";
                break;
            case 'R':
                status = setPosition(new Position(this.position.x, this.position.y+1));
                message = "Moved right!";
                break;
            default:
                message = "Invalid direction!";
                break;
        }

        if (!status){
            message = message +  "\nSomething has happened...\n";
        }

        //returns message customised to results of movement
        return message;

    }

    //sets the position of a player
    public boolean setPosition(Position P){

        Map mapGenerator = new Map();
        // checking if the position in which the player is, is a valid tile
        // if it is, return true
        // if it is not, return false

        // checking for negative tile coordinates
        if (P.x < 0 || P.y < 0) {
            System.out.println("Cannot have negative values! You lost your turn!");
            return false;
        }
        // checking if tile is out of bounds
        else if (P.x >= this.map.length||P.y >= this.map[0].length){
            System.out.println("Position is out of Bounds! You lost your turn!");
            return false;
        }

        char type = 'a';

        try {
            type = mapGenerator.getTileType(P.x,P.y,this.map);
        } catch (final MyInvalidParameterException e){
            System.out.println(e.getMessage());
        }

        // checking if the tile is grass, water or treasure
        if(type=='g'|| type=='c'){
            this.position = P;
            return true;
        }
        else if (type=='w'){
            this.position = P;
            System.out.println("You have reached water!");
            return true;
        }
        else if (type=='t'){
            this.position = P;
            System.out.println("You have reached treasure!");
            return true;
        }

        return false;

        //returns true if the tile is valid and has a valid type
        //returns false otherwise

    }
    @Override
    public String toString(){
        return "\nPosition x: "+ this.position.x +
                ", Position y: "+ this.position.y;
    }
}
