import java.io.BufferedWriter;
import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Game {

    //Game constructor
    public Game(MapType myMap, int x, int y, int num) {
        myMap = myMap.generate(x, y, num); // generating map for game
        this.map = myMap;
        this.maxTurns = (int) Math.ceil(2 * (x * y) / num); // setting a limit number of turns
        this.turns = new Turn[maxTurns];
        this.players = new Player[num];
        this.teamObs = new TeamPositionObserver();
    }

    private Turn turns[];
    private Player players[];
    public MapType map;
    final private int maxTurns;
    final private Observer teamObs;

    //getter and setter for players array
    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    //getter for team observer
    public Observer getTeamObs() {
        return teamObs;
    }

    //getter for turns array (used only for testing since variable is private)
    public Turn[] getTurns() {
        return turns;
    }

    //copies contents of a map grid onto another map grid variable
    public char[][] copy_paste_map(char[][] source, char[][] destination) {
        for (int s = 0; s < source.length; s++) {
            for (int t = 0; t < source[0].length; t++) {
                char inpt = source[s][t];
                {destination[s][t] = inpt;}
            }
        }

        return destination;
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        try {

            //displaying title to start accepting initial setting for the game
            System.out.println("\n ~~~ P I R A T E   R A C E ~~~\n\n");

            //asking user to enter the number of players, number of teams and the size of the map
            System.out.println("Enter the number of players:");
            int numOfPlayers = sc.nextInt();

            System.out.println("Enter the number of teams:");
            int numOfTeams = sc.nextInt();

            //checking number of players and matching number of teams
            if (numOfPlayers < 0 || numOfPlayers > 99)
                throw new IOException("Invalid input for \"Number of Players\"");

            if (numOfTeams > numOfPlayers)
                numOfTeams = numOfPlayers;

            System.out.println("Enter the size of the x-axis of the Map:");
            int MapSizeX = sc.nextInt();
            System.out.println("Enter the size of the y-axis of the Map:");
            int MapSizeY = sc.nextInt();

            //checking size of the map
            if (MapSizeX * MapSizeY <= 0 || MapSizeX * MapSizeY > 3000) {
                throw new IOException("Invalid input for \"Map size\"");
            }
            //asking user for type of map
            System.out.println("Would you like a Safe map (S) or a Hazardous Map, (H)?");
            char charType = sc.next().charAt(0);

            Type mapType;

            //setting the map type
            if (charType == 'S') {
                mapType = Type.Safe;
            } else if (charType == 'H') {
                mapType = Type.Hazardous;
            } else {
                throw new MyInvalidInputException("Map type does not exist!");
            }


            //setting the map
            Map mapGenerator = new Map();
            MapType myOriginalMap = mapGenerator.getMapType(mapType, MapSizeX, MapSizeY);

            //creating a game instance
            Game myGame = new Game(myOriginalMap, MapSizeX, MapSizeY, numOfPlayers);

            //checking the number of players is correct
            boolean checkNumPlayers = myGame.setNumPlayers(numOfPlayers);

            if (!checkNumPlayers) {
                throw new MyInvalidInputException("The number of players has to be in the range 2-8!");
            }

            //setting the players and their maps for the game instance
            for (int n = 0; n < numOfPlayers; n++) {
                myGame.players[n] = new Player();
                myGame.players[n].map = myOriginalMap.getGrid(); //is returning a null pointer why???
            }

            //sort players into teams
            int t = 0;
            for (int p = 0; p < numOfPlayers; p++){
                myGame.players[p].teamNo = t;
                if (t < numOfTeams-1)
                    t++;
                else
                    t=0;
            }

            //starting the game
            myGame.startGame(numOfPlayers, mapGenerator);

        } // catching any exceptions thrown in this method
        catch (final ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        } catch (final IOException e) {
            System.out.println("Invalid input: " + e.getMessage());
        } catch (final MyInvalidInputException e) {
            System.out.println(e.message());
        }
    }

    //prepares game created in main for playing
    public void startGame(int numOfPlayers, Map mapGenerator) {

        //creating arrays to store the initial positions of players
        int InitialPlayerPosX[] = new int[numOfPlayers];
        int InitialPlayerPosY[] = new int[numOfPlayers];
        boolean checkPosition;

        turns[0] = new Turn(numOfPlayers);

        //Getting the starting positions for each player
        for (int i = 0; i < numOfPlayers; i++) {

            do {
                //getting random positions for the players to start on
                InitialPlayerPosX[i] = (int) Math.floor(Math.random() * (players[i].getMap().length));
                InitialPlayerPosY[i] = (int) Math.floor(Math.random() * (players[i].getMap()[0].length));

                Position newPosition = new Position(InitialPlayerPosX[i], InitialPlayerPosY[i]);

                //checkPosition is true for either 'w','t','g','c'
                checkPosition = players[i].setPosition(newPosition);

                //Please note that this method prints if water/treasure is reached.
                //The loop repeats itself if the position is not grass
                //Therefore, please ignore the warning "You have reached water!"
                // when the initial positions are being set, since this is not true

                //further checking required to make sure not on 't' or 'w'

                char type = 'a';

                try {
                    //getting the tile type of this initial position
                    type = mapGenerator.getTileType(newPosition.x, newPosition.y, this.map.getGrid());
                } catch (final MyInvalidParameterException e) {
                    System.out.println(e.message());
                }

                if (type == 'w' || type == 't')
                    checkPosition = false;


            } while (!checkPosition); //repeating random position generation if current is invalid

            //displaying current player information
            System.out.println("Player " + (i + 1) + " (Team " + players[i].teamNo + ") position x, y: " + InitialPlayerPosX[i] + " " + InitialPlayerPosY[i]);

            try {
                //saving initial positions into first turn instance
                turns[0].addPosition(new Position(InitialPlayerPosX[i], InitialPlayerPosY[i]), i);
            } catch (final MyInvalidParameterException e) {
                System.out.println(e.message());
            }
        }

        //generating html maps for initial positions of players
        char[][][] initialMaps = new char[numOfPlayers][map.getMapSize()[0]][map.getMapSize()[1]];
        int[][] initialPositions = new int[numOfPlayers][2];
        for (int m = 0; m < numOfPlayers; m++){
            initialMaps[m] = copy_paste_map(initialMaps[m], map.getGrid());
            initialPositions[m][0] = InitialPlayerPosX[m];
            initialPositions[m][1] = InitialPlayerPosY[m];
        }

        generateHTMLFiles(initialMaps, initialPositions, mapGenerator);

        //starting gameplay
        playGame(numOfPlayers,mapGenerator, InitialPlayerPosX, InitialPlayerPosY);
    }

    //lets players play the game
    private void playGame(int numOfPlayers,Map mapGenerator, int InitialPlayerPosX[], int InitialPlayerPosY[]) {

        //variable to hold turn counter / number
        int turn = 1;

        //list of the new positions of the players after each turn
        int new_pos[][] = new int[numOfPlayers][2];

        //array for the winners (since there can be more than one)
        int winner[] = new int[numOfPlayers];

        //initializing array of maps variable to save all grids (for html map processing)
        char[][] arrayOfMaps[] = new char[numOfPlayers][map.getMapSize()[0]][map.getMapSize()[1]];
        for (int s = 0; s < arrayOfMaps.length; s++) {
            for (char[] row: arrayOfMaps[s]) {
                Arrays.fill(row, '\0');
            }
        }

        //copying the original map into the array of maps
        for (int m = 0; m < numOfPlayers; m++) {
            if (map.getType() == Type.Safe) {
                arrayOfMaps[m] = copy_paste_map(SafeMap.getmap().getGrid(), arrayOfMaps[m]);
            } else if (map.getType() == Type.Hazardous) {
                arrayOfMaps[m] = copy_paste_map(HazardousMap.getmap().getGrid(), arrayOfMaps[m]);
            }
        }

        try {

            //allowing the user to play the game and choose a direction to move in
            do { // going  through turns
                if (turn >= maxTurns)
                    throw new ArrayIndexOutOfBoundsException("Maximum turns reached, game ends here!");

                turns[turn] = new Turn(numOfPlayers);

                System.out.println("\nTurn number " + turn + " : \n");

                for (int i = 0; i < numOfPlayers; i++) {

                    //getting directions from the players
                    acceptingDirections(i);

                    //saving these positions into the array of new positions (for html map processing)
                    new_pos[i][0] = players[i].position.x;
                    new_pos[i][1] = players[i].position.y;

                    //saving the positions into the current positions of players in current turn
                    turns[turn].addPosition(players[i].position, i);

                    char typeOfCurr = mapGenerator.getTileType(players[i].position.x, players[i].position.y, players[i].getMap());

                    //checking the tile type for treasure or water tiles
                    if (typeOfCurr == 't') {
                        winner[i] = 1;
                    } else if (typeOfCurr == 'w') {
                        System.out.println("You loose, please return to original position");
                        //resetting to original positions if water was hit
                        players[i].position.x = InitialPlayerPosX[i];
                        players[i].position.y = InitialPlayerPosY[i];
                    }

                    // ~~~ for HTML ~~~

                    System.out.println("\nPlayer " + (i + 1) + " (Team " + players[i].teamNo + ") : \n");

                    //saving the maps into the appropriate player maps (team setting)
                    for (int j2 = 0; j2 < turn; j2++) {
                        int posX = turns[j2].getPosition(i).x;
                        int posY = turns[j2].getPosition(i).y;

                        saveMaps(mapGenerator, posX, posY, i, arrayOfMaps);
                    }

                    //saving the new map in the array of maps for HTML
                    arrayOfMaps[i] = copy_paste_map(players[i].map, arrayOfMaps[i]);
                }

                //generating html
                generateHTMLFiles(arrayOfMaps, new_pos, mapGenerator);

                //for after HTML
                for (int k = 0; k < numOfPlayers; k++) { //changing the player position tile to covered
                    turns[turn].addPosition(players[k].position, k);
                }

                boolean won = false;

                // returning if at least one person found the treasure
                for (int m = 0; m < numOfPlayers; m++) {
                    if (winner[m] == 1) {
                        System.out.println("Player " + (m + 1) + "(Team " + players[m].teamNo + ") you are the winner after " + turn + " turns!!");
                        won = true;
                    }
                }
                turn++;

                //exiting loop if game was won
                if (won)
                    return;

            } while (true);

        } catch (final MyInvalidParameterException e) {
            System.out.println(e.message());
        }

    }

    //accepts user input for the player's next directions
    private void acceptingDirections(int i){
        Scanner sc = new Scanner(System.in);
        char direction;
        String loc;

        do {
            do {
                //asking and getting directions
                System.out.println("\nPlayer " + (i + 1) + "(Team " + players[i].teamNo + ")");
                System.out.println("Enter the direction you want to move:\nU: Up\nD: Down\nL: Left\nR: Right ");
                direction = sc.next().charAt(0);
                System.out.println(direction);
            } while (direction != 'U' && direction != 'D' && direction != 'L' && direction != 'R');
            loc = players[i].move(direction);

            //printing what happened
            System.out.println(loc);

            // repeating the direction entry if something happened (out of bounds)
        } while (loc.equals("Invalid direction!\nSomething has happened...\n"));

    }

    //saves the maps of the players (called when players have moved / positions were changed)
    private void saveMaps(Map mapGenerator, int posX, int posY, int i, char[][][] arrayOfMaps){

        try {
            char check = mapGenerator.getTileType(posX, posY, arrayOfMaps[i]);

            //saving positions and covered spaces using team observer
            if (check != 'w' && check != 't') {
                this.teamObs.update(this.players, i, posX, posY, arrayOfMaps);
            }

        } catch (final MyInvalidParameterException e) {
            System.out.println(e.message());
        }
    }

    //sets the number of players for the game
    public boolean setNumPlayers(int n) {

        // checking number of players is valid
        if (n >= 2 && n <= 8) {
            this.players = new Player[n];
            return true;
        } else {
            return false;
        }
    }

    //this method is called after each turn and new positions are updated,
    //to create html files for displaying to players
    public void generateHTMLFiles(char[][] arrayOfMaps[], int new_pos[][], Map mapGenerator) {
        //array of maps = map for each player

        int x_size = arrayOfMaps[0].length; // map size
        int y_size = arrayOfMaps[0][0].length;

        int no_of_players = arrayOfMaps.length;

        for (int p = 0; p < no_of_players; p++) { // creating a file for each player

//            boolean[] teamMates = new boolean[players.length];
//
//            //saving teammates
//            for (int q = 0; q < players.length; q++){
//                teamMates[q] = false;
//                if (players[q].getTeamNo() == players[p].getTeamNo())
//                    teamMates[q] = true;
//            }

//            int[] teamMates = new int[players.length];
//            int count = 0;
//            for (int p2 = 0; p2 < players.length; p2++){
//                if (players[p2].getTeamNo() == players[p].getTeamNo()){
//                    teamMates[count] = p2;
//                    count++;
//                }
//            }

            String htmlTable[][] = new String[x_size][y_size]; // table for storing html code for each tile

            //initial html code
            String html = "<h1>Map of player " + (p + 1) + " - Team " + players[p].getTeamNo() + "</h1>\n" +
                    "<head>\n" +
                    "<style>\n" +
                    "table, th, td {\n" +
                    "  border: 1px solid black;\n" +
                    "  border-collapse: collapse;\n" +
                    "  background-color: grey;\n" +
                    "}\n" +
                    "th, td {\n" +
                    "  padding: 10px;\n" +
                    "  text-align: centre;\n" + "}\n" +
                    "</style>\n" +
                    "</head>\n" +
                    "<table style=\"width:50%\">";

            for (int x = 0; x < x_size; x++) { // going through rows of table

                html = html + "\t<tr>\n"; // starting html table row

                for (int y = 0; y < y_size; y++) { // going through elements of row

                    char currentPos = 'a';

                    try { // getting tile type of current tile
                        currentPos = mapGenerator.getTileType(x, y, arrayOfMaps[p]);
                    } catch (final MyInvalidParameterException e) {
                        System.out.println(e.message());
                    }

                    // colouring of the current tile is in the new position
                    if (x == new_pos[p][0] && y == new_pos[p][1]) {

//                        //looking for teammates
//                        for (int r = 0; r < players.length; r++){
//                            if (teamMates[r]){

                                char player = 'Y';

                                //setting the current position box to be the colour according to the tile type
                                switch (currentPos) { // getTileType
                                    case 'g':
                                        htmlTable[x][y] = "\t\t<td style=\"background-color:green;color:white;\"> " + player + " </td>\n";
                                        break;
                                    case 'w':
                                        htmlTable[x][y] = "\t\t<td style=\"background-color:blue;color:white;\"> " + player + " </td>\n";
                                        break;
                                    case 't':
                                        htmlTable[x][y] = "\t\t<td style=\"background-color:gold;color:white;\"> " + player + " </td>\n";
                                        break;
                                    case 'c':
                                        htmlTable[x][y] = "\t\t<td style=\"background-color:green;color:white;\"> " + player + " </td>\n";
                                        break;
                                }
                            //}
                        //}
                    }

                    else if (currentPos == 'c') // if the space is already c, then it must have been covered before, therefore it must be green
                        htmlTable[x][y] = "\t\t<td style=\"background-color:green;color:white;\"> - </td>\n";

                    else { // all other uncovered spaces are grey
                        htmlTable[x][y] = "\t\t<td> ? </td>\n";
                    }

                    html = html + htmlTable[x][y]; // adding this to the html string

                }
//                html = html + "\t</tr>\n"
//                        + "\t<p>\n"
//                        + "\t\tY = Your move\n"
//                        + "\t\tGreen space = Grass\n"
//                        + "\t\tBlue space = Water\n"
//                        + "\t\tGold space = Treasure\n" +
//                        "\t</p>\n";
            }

            //creating the html file for this player and writing our html code
            File htmlFile = new File("map_player_" + (p + 1) + ".html");
            try {
                BufferedWriter bufferW = new BufferedWriter(new FileWriter(htmlFile));
                bufferW.write(html);
                bufferW.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}